/** ###################################################################
 **     Filename    : NVIC_Init.c
 **     Processor   : MKE04Z128LD4
 **     Abstract    :
 **          This file implements the NVIC module initialization
 **          according to the Peripheral Initialization settings, and
 **          defines interrupt service routines prototypes.
 **
 **     Contents    :
 **         Int   - void NVIC_Init(void);
 **
 **     Copyright : 1997 - 2014 Freescale Semiconductor, Inc.
 **     All Rights Reserved.
 **
 **     Redistribution and use in source and binary forms, with or without modification,
 **     are permitted provided that the following conditions are met:
 **
 **     o Redistributions of source code must retain the above copyright notice, this list
 **       of conditions and the following disclaimer.
 **
 **     o Redistributions in binary form must reproduce the above copyright notice, this
 **       list of conditions and the following disclaimer in the documentation and/or
 **       other materials provided with the distribution.
 **
 **     o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 **       contributors may be used to endorse or promote products derived from this
 **       software without specific prior written permission.
 **
 **     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 **     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 **     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 **     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 **     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 **     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 **     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 **     ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 **     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 **     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 **     http: www.freescale.com
 **     mail: support@freescale.com
 ** ###################################################################*/

/*!
 * @file NVIC_Init.c
 * @brief This file implements the NVIC module initialization according to the
 *        Peripheral Initialization settings, and defines interrupt service
 *        routines prototypes.
 */


/* MODULE NVIC_Init. */

#include "NVIC_Init.h"
#include "MKE04Z1284.h"
#include "Init_Config.h"

#ifdef __cplusplus
extern "C" {
#endif

void NVIC_Init(void) {

  /* Register 'NVIC_IPR1' initialization */
  #ifdef NVIC_IPR1_VALUE
  NVIC_IPR1 = NVIC_IPR1_VALUE;
  #endif

  /* Register 'NVIC_IPR2' initialization */
  #ifdef NVIC_IPR2_VALUE
  NVIC_IPR2 = NVIC_IPR2_VALUE;
  #endif

  /* Register 'NVIC_IPR3' initialization */
  #ifdef NVIC_IPR3_VALUE
  NVIC_IPR3 = NVIC_IPR3_VALUE;
  #endif

  /* Register 'NVIC_IPR4' initialization */
  #ifdef NVIC_IPR4_VALUE
  NVIC_IPR4 = NVIC_IPR4_VALUE;
  #endif

  /* Register 'NVIC_IPR5' initialization */
  #ifdef NVIC_IPR5_VALUE
  NVIC_IPR5 = NVIC_IPR5_VALUE;
  #endif

  /* Register 'NVIC_IPR6' initialization */
  #ifdef NVIC_IPR6_VALUE
  NVIC_IPR6 = NVIC_IPR6_VALUE;
  #endif

  /* Register 'NVIC_IPR7' initialization */
  #ifdef NVIC_IPR7_VALUE
  NVIC_IPR7 = NVIC_IPR7_VALUE;
  #endif

  NVIC_IPR0 = 0;

  /* Register 'NVIC_ICPR' initialization */
  #ifdef NVIC_ICPR_VALUE
  NVIC_ICPR = NVIC_ICPR_VALUE;
  #endif

  /* Register 'NVIC_ISPR' initialization */
  #ifdef NVIC_ISPR_VALUE
  NVIC_ISPR = NVIC_ISPR_VALUE;
  #endif

  /* Register 'NVIC_ISER' initialization */
  #ifdef NVIC_ISER_VALUE
  NVIC_ISER = NVIC_ISER_VALUE;
  #endif

  /* Register 'NVIC_ICER' initialization */
  #ifdef NVIC_ICER_VALUE
  NVIC_ICER = NVIC_ICER_VALUE;
  #endif
}

#ifdef __cplusplus
}
#endif

/* END NVIC_Init. */

/** ###################################################################
 **
 **     This file is a part of Processor Expert static initialization
 **     library for Freescale microcontrollers.
 **
 ** ################################################################### */
