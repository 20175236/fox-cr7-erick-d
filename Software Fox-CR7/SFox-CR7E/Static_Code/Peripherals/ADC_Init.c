/** ###################################################################
 **     Filename    : ADC_Init.c
 **     Processor   : MKE04Z128LD4
 **     Abstract    :
 **          This file implements the ADC module initialization
 **          according to the Peripheral Initialization settings, and
 **          defines interrupt service routines prototypes.
 **
 **     Contents    :
 **         Int   - void ADC_Init(void);
 **
 **     Copyright : 1997 - 2014 Freescale Semiconductor, Inc.
 **     All Rights Reserved.
 **
 **     Redistribution and use in source and binary forms, with or without modification,
 **     are permitted provided that the following conditions are met:
 **
 **     o Redistributions of source code must retain the above copyright notice, this list
 **       of conditions and the following disclaimer.
 **
 **     o Redistributions in binary form must reproduce the above copyright notice, this
 **       list of conditions and the following disclaimer in the documentation and/or
 **       other materials provided with the distribution.
 **
 **     o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 **       contributors may be used to endorse or promote products derived from this
 **       software without specific prior written permission.
 **
 **     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 **     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 **     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 **     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 **     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 **     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 **     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 **     ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 **     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 **     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 **     http: www.freescale.com
 **     mail: support@freescale.com
 ** ###################################################################*/

/*!
 * @file ADC_Init.c
 * @brief This file implements the ADC module initialization according to the
 *        Peripheral Initialization settings, and defines interrupt service
 *        routines prototypes.
 */


/* MODULE ADC_Init. */

#include "ADC_Init.h"
#include "MKE04Z1284.h"
#include "Init_Config.h"

#ifdef __cplusplus
extern "C" {
#endif

void ADC_Init(void) {
  /* Filling up the channel FIFO if the Measurement preparation is selected */
  #ifdef ADC_INITIALIZATION_VALUES
    uint8_t i = 0U;
    const uint8_t ChannelFIFO[ADC_FIFO_INPUTS_NUM] = ADC_INITIALIZATION_VALUES;
  #endif

  /* Register 'ADC_APCTL1' initialization */
  #ifdef ADC_APCTL1_VALUE
  ADC_APCTL1 = ADC_APCTL1_VALUE;
  #endif

  /* Register 'ADC_SC4' initialization */
  #ifdef ADC_SC4_VALUE
  ADC_SC4 = ADC_SC4_VALUE;
  #endif

  /* Register 'ADC_SC5' initialization */
  #ifdef ADC_SC5_VALUE
  ADC_SC5 = ADC_SC5_VALUE;
  #endif

  /* Register 'ADC_CV' initialization */
  #ifdef ADC_CV_VALUE
  ADC_CV = ADC_CV_VALUE;
  #endif

  /* Register 'ADC_SC3' initialization */
  #ifdef ADC_SC3_VALUE
  ADC_SC3 = ADC_SC3_VALUE;
  #endif

  /* Register 'ADC_SC2' initialization */
  #if ADC_SC2_MASK
    #if ADC_SC2_MASK == 0xFFFFFFFF
  ADC_SC2 = ADC_SC2_VALUE;
    #elif ADC_SC2_MASK == ADC_SC2_VALUE
  ADC_SC2 |= ADC_SC2_VALUE;
    #elif ADC_SC2_VALUE == 0
  ADC_SC2 &= ~ADC_SC2_MASK;
    #else
  ADC_SC2 = (ADC_SC2 & (~ADC_SC2_MASK)) | ADC_SC2_VALUE;
    #endif
  #elif defined(ADC_SC2_VALUE)
  ADC_SC2 = ADC_SC2_VALUE;
  #endif

  /* Filling up the channel FIFO if the Measurement preparation is selected */
  #ifdef ADC_DISABLE_CONVERSIONS
    ADC_SC1 = ADC_CONVERSION_COMPLETE_INT | ADC_CONVERSION_MODE | ADC_DISABLE_CONVERSIONS; /* Setup ADC_SC1 register for disable conversions */
  #elif defined(ADC_INITIALIZATION_VALUES)
    for (i = 0U; i < ADC_FIFO_INPUTS_NUM; i++) {
      ADC_SC1 = ADC_CONVERSION_COMPLETE_INT | ADC_CONVERSION_MODE | ChannelFIFO[i]; /* Fillup channel FIFO */
    }
  #endif
}

#ifdef __cplusplus
}
#endif

/* END ADC_Init. */

/** ###################################################################
 **
 **     This file is a part of Processor Expert static initialization
 **     library for Freescale microcontrollers.
 **
 ** ################################################################### */
