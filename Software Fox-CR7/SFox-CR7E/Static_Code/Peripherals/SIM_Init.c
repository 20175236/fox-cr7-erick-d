/** ###################################################################
 **     Filename    : SIM_Init.c
 **     Processor   : MKE04Z128LD4
 **     Abstract    :
 **          This file implements the SIM module initialization
 **          according to the Peripheral Initialization settings, and
 **          defines interrupt service routines prototypes.
 **
 **     Contents    :
 **         Int   - void SIM_Init(void);
 **
 **     Copyright : 1997 - 2014 Freescale Semiconductor, Inc.
 **     All Rights Reserved.
 **
 **     Redistribution and use in source and binary forms, with or without modification,
 **     are permitted provided that the following conditions are met:
 **
 **     o Redistributions of source code must retain the above copyright notice, this list
 **       of conditions and the following disclaimer.
 **
 **     o Redistributions in binary form must reproduce the above copyright notice, this
 **       list of conditions and the following disclaimer in the documentation and/or
 **       other materials provided with the distribution.
 **
 **     o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 **       contributors may be used to endorse or promote products derived from this
 **       software without specific prior written permission.
 **
 **     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 **     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 **     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 **     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 **     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 **     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 **     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 **     ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 **     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 **     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 **     http: www.freescale.com
 **     mail: support@freescale.com
 ** ###################################################################*/

/*!
 * @file SIM_Init.c
 * @brief This file implements the SIM module initialization according to the
 *        Peripheral Initialization settings, and defines interrupt service
 *        routines prototypes.
 */


/* MODULE SIM_Init. */

#include "SIM_Init.h"
#include "MKE04Z1284.h"
#include "Init_Config.h"

#ifdef __cplusplus
extern "C" {
#endif

void SIM_Init(void) {

  /* Register 'SIM_SCGC' initialization */
  #if SIM_SCGC_MASK
    #if SIM_SCGC_MASK == 0xFFFFFFFF
  SIM_SCGC = SIM_SCGC_VALUE;
    #elif SIM_SCGC_MASK == SIM_SCGC_VALUE
  SIM_SCGC |= SIM_SCGC_VALUE;
    #elif SIM_SCGC_VALUE == 0
  SIM_SCGC &= ~SIM_SCGC_MASK;
    #else
  SIM_SCGC = (SIM_SCGC & (~SIM_SCGC_MASK)) | SIM_SCGC_VALUE;
    #endif
  #elif defined(SIM_SCGC_VALUE)
  SIM_SCGC = SIM_SCGC_VALUE;
  #endif

  /* Register 'SIM_CLKDIV' initialization */
  #ifdef SIM_CLKDIV_VALUE
  SIM_CLKDIV = SIM_CLKDIV_VALUE;
  #endif

  /* Register 'SIM_SOPT0' initialization */
  #ifdef SIM_SOPT0_VALUE
  SIM_SOPT0 = SIM_SOPT0_VALUE;
  #endif

  /* Register 'SIM_SOPT1' initialization */
  #ifdef SIM_SOPT1_VALUE
  SIM_SOPT1 = SIM_SOPT1_VALUE;
  #endif

  /* Register 'SIM_PINSEL0' initialization */
  #ifdef SIM_PINSEL0_VALUE
  SIM_PINSEL0 = SIM_PINSEL0_VALUE;
  #endif

  /* Register 'SIM_PINSEL1' initialization */
  #ifdef SIM_PINSEL1_VALUE
  SIM_PINSEL1 = SIM_PINSEL1_VALUE;
  #endif
}

#ifdef __cplusplus
}
#endif

/* END SIM_Init. */

/** ###################################################################
 **
 **     This file is a part of Processor Expert static initialization
 **     library for Freescale microcontrollers.
 **
 ** ################################################################### */
