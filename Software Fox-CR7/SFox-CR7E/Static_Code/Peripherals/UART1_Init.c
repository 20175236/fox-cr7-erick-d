/** ###################################################################
 **     Filename    : UART1_Init.c
 **     Processor   : MKE04Z128LD4
 **     Abstract    :
 **          This file implements the UART1 module initialization
 **          according to the Peripheral Initialization settings, and
 **          defines interrupt service routines prototypes.
 **
 **     Contents    :
 **         Int   - void UART1_Init(void);
 **
 **     Copyright : 1997 - 2014 Freescale Semiconductor, Inc.
 **     All Rights Reserved.
 **
 **     Redistribution and use in source and binary forms, with or without modification,
 **     are permitted provided that the following conditions are met:
 **
 **     o Redistributions of source code must retain the above copyright notice, this list
 **       of conditions and the following disclaimer.
 **
 **     o Redistributions in binary form must reproduce the above copyright notice, this
 **       list of conditions and the following disclaimer in the documentation and/or
 **       other materials provided with the distribution.
 **
 **     o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 **       contributors may be used to endorse or promote products derived from this
 **       software without specific prior written permission.
 **
 **     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 **     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 **     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 **     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 **     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 **     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 **     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 **     ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 **     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 **     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 **     http: www.freescale.com
 **     mail: support@freescale.com
 ** ###################################################################*/

/*!
 * @file UART1_Init.c
 * @brief This file implements the UART1 module initialization according to the
 *        Peripheral Initialization settings, and defines interrupt service
 *        routines prototypes.
 */


/* MODULE UART1_Init. */

#include "UART1_Init.h"
#include "MKE04Z1284.h"
#include "Init_Config.h"

#ifdef __cplusplus
extern "C" {
#endif

void UART1_Init(void) {

  /* Register 'UART1_C2' initialization */
  #ifdef UART1_C2_VALUE_1
  UART1_C2 = UART1_C2_VALUE_1;
  #endif

  /* Register 'UART1_BDH' initialization */
  #ifdef UART1_BDH_VALUE
  UART1_BDH = UART1_BDH_VALUE;
  #endif

  /* Register 'UART1_BDL' initialization */
  #ifdef UART1_BDL_VALUE
  UART1_BDL = UART1_BDL_VALUE;
  #endif

  /* Register 'UART1_C1' initialization */
  #ifdef UART1_C1_VALUE
  UART1_C1 = UART1_C1_VALUE;
  #endif

  /* Register 'UART1_S2' initialization */
  #ifdef UART1_S2_VALUE
  UART1_S2 = UART1_S2_VALUE;
  #endif

  /* Clearing of the Rx flags */
  #ifdef UART1_CLEAR_RX_FLAGS
  (void)UART1_S1; /* Dummy read of the UART1_S1 register to clear flags */
  (void)UART1_D;  /* Dummy read of the UART1_D register to clear flags */
  #endif

  /* Register 'UART1_C3' initialization */
  #ifdef UART1_C3_VALUE
  UART1_C3 = UART1_C3_VALUE;
  #endif

  /* Register 'UART1_C2' initialization */
  #ifdef UART1_C2_VALUE_2
  UART1_C2 = UART1_C2_VALUE_2;
  #endif
}

#ifdef __cplusplus
}
#endif

/* END UART1_Init. */

/** ###################################################################
 **
 **     This file is a part of Processor Expert static initialization
 **     library for Freescale microcontrollers.
 **
 ** ################################################################### */
