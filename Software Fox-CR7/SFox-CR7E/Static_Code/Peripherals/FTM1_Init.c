/** ###################################################################
 **     Filename    : FTM1_Init.c
 **     Processor   : MKE04Z128LD4
 **     Abstract    :
 **          This file implements the FTM1 module initialization
 **          according to the Peripheral Initialization settings, and
 **          defines interrupt service routines prototypes.
 **
 **     Contents    :
 **         Int   - void FTM1_Init(void);
 **
 **     Copyright : 1997 - 2014 Freescale Semiconductor, Inc.
 **     All Rights Reserved.
 **
 **     Redistribution and use in source and binary forms, with or without modification,
 **     are permitted provided that the following conditions are met:
 **
 **     o Redistributions of source code must retain the above copyright notice, this list
 **       of conditions and the following disclaimer.
 **
 **     o Redistributions in binary form must reproduce the above copyright notice, this
 **       list of conditions and the following disclaimer in the documentation and/or
 **       other materials provided with the distribution.
 **
 **     o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 **       contributors may be used to endorse or promote products derived from this
 **       software without specific prior written permission.
 **
 **     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 **     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 **     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 **     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 **     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 **     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 **     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 **     ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 **     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 **     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 **     http: www.freescale.com
 **     mail: support@freescale.com
 ** ###################################################################*/

/*!
 * @file FTM1_Init.c
 * @brief This file implements the FTM1 module initialization according to the
 *        Peripheral Initialization settings, and defines interrupt service
 *        routines prototypes.
 */


/* MODULE FTM1_Init. */

#include "FTM1_Init.h"
#include "MKE04Z1284.h"
#include "Init_Config.h"

#ifdef __cplusplus
extern "C" {
#endif

void FTM1_Init(void) {
  (void)FTM1_SC;  /* Dummy read of the Control Status register to clear the interrupt flag */
  FTM1_SC = 0;  /* Stop the counter */
  #ifdef FTM1_C0SC
  (void)FTM1_C0SC; /* Dummy read of the Channel Status and Control 0 register to clear the interrupt flag */
  #endif
  #ifdef FTM1_C1SC
  (void)FTM1_C1SC; /* Dummy read of the Channel Status and Control 1 register to clear the interrupt flag */
  #endif
  #ifdef FTM1_C2SC
  (void)FTM1_C2SC; /* Dummy read of the Channel Status and Control 2 register to clear the interrupt flag */
  #endif
  #ifdef FTM1_C3SC
  (void)FTM1_C3SC; /* Dummy read of the Channel Status and Control 3 register to clear the interrupt flag */
  #endif
  #ifdef FTM1_C4SC
  (void)FTM1_C4SC; /* Dummy read of the Channel Status and Control 4 register to clear the interrupt flag */
  #endif
  #ifdef FTM1_C5SC
  (void)FTM1_C5SC; /* Dummy read of the Channel Status and Control 5 register to clear the interrupt flag */
  #endif
  #ifdef FTM1_C6SC
  (void)FTM1_C6SC; /* Dummy read of the Channel Status and Control 6 register to clear the interrupt flag */
  #endif
  #ifdef FTM1_C7SC
  (void)FTM1_C7SC; /* Dummy read of the Channel Status and Control 7 register to clear the interrupt flag */
  #endif

  FTM1_C0SC = 0;

  FTM1_C1SC = 0;

  /* Register 'FTM1_SC' initialization */
  #if FTM1_SC_MASK_2
    #if FTM1_SC_MASK_2 == 0xFFFFFFFF
  FTM1_SC = FTM1_SC_VALUE_2;
    #elif FTM1_SC_MASK_2 == FTM1_SC_VALUE_2
  FTM1_SC |= FTM1_SC_VALUE_2;
    #elif FTM1_SC_VALUE_2 == 0
  FTM1_SC &= ~FTM1_SC_MASK_2;
    #else
  FTM1_SC = (FTM1_SC & (~FTM1_SC_MASK_2)) | FTM1_SC_VALUE_2;
    #endif
  #elif defined(FTM1_SC_VALUE_2)
  FTM1_SC = FTM1_SC_VALUE_2;
  #endif

  /* Register 'FTM1_C0SC' initialization */
  #if FTM1_C0SC_MASK
    #if FTM1_C0SC_MASK == 0xFFFFFFFF
  FTM1_C0SC = FTM1_C0SC_VALUE;
    #elif FTM1_C0SC_MASK == FTM1_C0SC_VALUE
  FTM1_C0SC |= FTM1_C0SC_VALUE;
    #elif FTM1_C0SC_VALUE == 0
  FTM1_C0SC &= ~FTM1_C0SC_MASK;
    #else
  FTM1_C0SC = (FTM1_C0SC & (~FTM1_C0SC_MASK)) | FTM1_C0SC_VALUE;
    #endif
  #elif defined(FTM1_C0SC_VALUE)
  FTM1_C0SC = FTM1_C0SC_VALUE;
  #endif

  /* Register 'FTM1_C1SC' initialization */
  #if FTM1_C1SC_MASK
    #if FTM1_C1SC_MASK == 0xFFFFFFFF
  FTM1_C1SC = FTM1_C1SC_VALUE;
    #elif FTM1_C1SC_MASK == FTM1_C1SC_VALUE
  FTM1_C1SC |= FTM1_C1SC_VALUE;
    #elif FTM1_C1SC_VALUE == 0
  FTM1_C1SC &= ~FTM1_C1SC_MASK;
    #else
  FTM1_C1SC = (FTM1_C1SC & (~FTM1_C1SC_MASK)) | FTM1_C1SC_VALUE;
    #endif
  #elif defined(FTM1_C1SC_VALUE)
  FTM1_C1SC = FTM1_C1SC_VALUE;
  #endif

  /* Register 'FTM1_C0V' initialization */
  #if FTM1_C0V_MASK
    #if FTM1_C0V_MASK == 0xFFFFFFFF
  FTM1_C0V = FTM1_C0V_VALUE;
    #elif FTM1_C0V_MASK == FTM1_C0V_VALUE
  FTM1_C0V |= FTM1_C0V_VALUE;
    #elif FTM1_C0V_VALUE == 0
  FTM1_C0V &= ~FTM1_C0V_MASK;
    #else
  FTM1_C0V = (FTM1_C0V & (~FTM1_C0V_MASK)) | FTM1_C0V_VALUE;
    #endif
  #elif defined(FTM1_C0V_VALUE)
  FTM1_C0V = FTM1_C0V_VALUE;
  #endif

  /* Register 'FTM1_C1V' initialization */
  #if FTM1_C1V_MASK
    #if FTM1_C1V_MASK == 0xFFFFFFFF
  FTM1_C1V = FTM1_C1V_VALUE;
    #elif FTM1_C1V_MASK == FTM1_C1V_VALUE
  FTM1_C1V |= FTM1_C1V_VALUE;
    #elif FTM1_C1V_VALUE == 0
  FTM1_C1V &= ~FTM1_C1V_MASK;
    #else
  FTM1_C1V = (FTM1_C1V & (~FTM1_C1V_MASK)) | FTM1_C1V_VALUE;
    #endif
  #elif defined(FTM1_C1V_VALUE)
  FTM1_C1V = FTM1_C1V_VALUE;
  #endif

  /* Register 'FTM1_MOD' initialization */
  #if FTM1_MOD_MASK
    #if FTM1_MOD_MASK == 0xFFFFFFFF
  FTM1_MOD = FTM1_MOD_VALUE;
    #elif FTM1_MOD_MASK == FTM1_MOD_VALUE
  FTM1_MOD |= FTM1_MOD_VALUE;
    #elif FTM1_MOD_VALUE == 0
  FTM1_MOD &= ~FTM1_MOD_MASK;
    #else
  FTM1_MOD = (FTM1_MOD & (~FTM1_MOD_MASK)) | FTM1_MOD_VALUE;
    #endif
  #elif defined(FTM1_MOD_VALUE)
  FTM1_MOD = FTM1_MOD_VALUE;
  #endif

  FTM1_CNT &= ~0xFFFF;

  /* Register 'FTM1_SC' initialization */
  #if FTM1_SC_MASK_3
    #if FTM1_SC_MASK_3 == 0xFFFFFFFF
  FTM1_SC = FTM1_SC_VALUE_3;
    #elif FTM1_SC_MASK_3 == FTM1_SC_VALUE_3
  FTM1_SC |= FTM1_SC_VALUE_3;
    #elif FTM1_SC_VALUE_3 == 0
  FTM1_SC &= ~FTM1_SC_MASK_3;
    #else
  FTM1_SC = (FTM1_SC & (~FTM1_SC_MASK_3)) | FTM1_SC_VALUE_3;
    #endif
  #elif defined(FTM1_SC_VALUE_3)
  FTM1_SC = FTM1_SC_VALUE_3;
  #endif
}

#ifdef __cplusplus
}
#endif

/* END FTM1_Init. */

/** ###################################################################
 **
 **     This file is a part of Processor Expert static initialization
 **     library for Freescale microcontrollers.
 **
 ** ################################################################### */
