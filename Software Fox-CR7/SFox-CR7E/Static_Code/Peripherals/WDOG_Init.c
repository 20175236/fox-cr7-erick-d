/** ###################################################################
 **     Filename    : WDOG_Init.c
 **     Processor   : MKE04Z128LD4
 **     Abstract    :
 **          This file implements the WDOG module initialization
 **          according to the Peripheral Initialization settings, and
 **          defines interrupt service routines prototypes.
 **
 **     Contents    :
 **         Int   - void WDOG_Init(void);
 **
 **     Copyright : 1997 - 2014 Freescale Semiconductor, Inc.
 **     All Rights Reserved.
 **
 **     Redistribution and use in source and binary forms, with or without modification,
 **     are permitted provided that the following conditions are met:
 **
 **     o Redistributions of source code must retain the above copyright notice, this list
 **       of conditions and the following disclaimer.
 **
 **     o Redistributions in binary form must reproduce the above copyright notice, this
 **       list of conditions and the following disclaimer in the documentation and/or
 **       other materials provided with the distribution.
 **
 **     o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 **       contributors may be used to endorse or promote products derived from this
 **       software without specific prior written permission.
 **
 **     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 **     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 **     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 **     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 **     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 **     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 **     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 **     ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 **     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 **     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 **     http: www.freescale.com
 **     mail: support@freescale.com
 ** ###################################################################*/

/*!
 * @file WDOG_Init.c
 * @brief This file implements the WDOG module initialization according to the
 *        Peripheral Initialization settings, and defines interrupt service
 *        routines prototypes.
 */


/* MODULE WDOG_Init. */

#include "WDOG_Init.h"
#include "MKE04Z1284.h"
#include "Init_Config.h"

#ifdef __cplusplus
extern "C" {
#endif

void WDOG_Init(void) {

  WDOG_CNT = 0x20C5;

  WDOG_CNT = 0x28D9;

  /* Register 'WDOG_WIN' initialization */
  #ifdef WDOG_WIN_VALUE
  WDOG_WIN = WDOG_WIN_VALUE;
  #endif

  /* Register 'WDOG_TOVAL' initialization */
  #ifdef WDOG_TOVAL_VALUE
  WDOG_TOVAL = WDOG_TOVAL_VALUE;
  #endif

  /* Register 'WDOG_CS2' initialization */
  #if WDOG_CS2_MASK
    #if WDOG_CS2_MASK == 0xFF
  WDOG_CS2 = WDOG_CS2_VALUE;
    #elif WDOG_CS2_MASK == WDOG_CS2_VALUE
  WDOG_CS2 |= WDOG_CS2_VALUE;
    #elif WDOG_CS2_VALUE == 0
  WDOG_CS2 &= ~WDOG_CS2_MASK;
    #else
  WDOG_CS2 = (WDOG_CS2 & (~WDOG_CS2_MASK)) | WDOG_CS2_VALUE;
    #endif
  #elif defined(WDOG_CS2_VALUE)
  WDOG_CS2 = WDOG_CS2_VALUE;
  #endif

  /* Register 'WDOG_CS1' initialization */
  #if WDOG_CS1_MASK
    #if WDOG_CS1_MASK == 0xFF
  WDOG_CS1 = WDOG_CS1_VALUE;
    #elif WDOG_CS1_MASK == WDOG_CS1_VALUE
  WDOG_CS1 |= WDOG_CS1_VALUE;
    #elif WDOG_CS1_VALUE == 0
  WDOG_CS1 &= ~WDOG_CS1_MASK;
    #else
  WDOG_CS1 = (WDOG_CS1 & (~WDOG_CS1_MASK)) | WDOG_CS1_VALUE;
    #endif
  #elif defined(WDOG_CS1_VALUE)
  WDOG_CS1 = WDOG_CS1_VALUE;
  #endif
}

#ifdef __cplusplus
}
#endif

/* END WDOG_Init. */

/** ###################################################################
 **
 **     This file is a part of Processor Expert static initialization
 **     library for Freescale microcontrollers.
 **
 ** ################################################################### */
