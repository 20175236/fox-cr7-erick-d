/** ###################################################################
 **     Filename    : UART2_Init.c
 **     Processor   : MKE04Z128LD4
 **     Abstract    :
 **          This file implements the UART2 module initialization
 **          according to the Peripheral Initialization settings, and
 **          defines interrupt service routines prototypes.
 **
 **     Contents    :
 **         Int   - void UART2_Init(void);
 **
 **     Copyright : 1997 - 2014 Freescale Semiconductor, Inc.
 **     All Rights Reserved.
 **
 **     Redistribution and use in source and binary forms, with or without modification,
 **     are permitted provided that the following conditions are met:
 **
 **     o Redistributions of source code must retain the above copyright notice, this list
 **       of conditions and the following disclaimer.
 **
 **     o Redistributions in binary form must reproduce the above copyright notice, this
 **       list of conditions and the following disclaimer in the documentation and/or
 **       other materials provided with the distribution.
 **
 **     o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 **       contributors may be used to endorse or promote products derived from this
 **       software without specific prior written permission.
 **
 **     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 **     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 **     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 **     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 **     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 **     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 **     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 **     ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 **     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 **     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 **     http: www.freescale.com
 **     mail: support@freescale.com
 ** ###################################################################*/

/*!
 * @file UART2_Init.c
 * @brief This file implements the UART2 module initialization according to the
 *        Peripheral Initialization settings, and defines interrupt service
 *        routines prototypes.
 */


/* MODULE UART2_Init. */

#include "UART2_Init.h"
#include "MKE04Z1284.h"
#include "Init_Config.h"

#ifdef __cplusplus
extern "C" {
#endif

void UART2_Init(void) {

  /* Register 'UART2_C2' initialization */
  #ifdef UART2_C2_VALUE_1
  UART2_C2 = UART2_C2_VALUE_1;
  #endif

  /* Register 'UART2_BDH' initialization */
  #ifdef UART2_BDH_VALUE
  UART2_BDH = UART2_BDH_VALUE;
  #endif

  /* Register 'UART2_BDL' initialization */
  #ifdef UART2_BDL_VALUE
  UART2_BDL = UART2_BDL_VALUE;
  #endif

  /* Register 'UART2_C1' initialization */
  #ifdef UART2_C1_VALUE
  UART2_C1 = UART2_C1_VALUE;
  #endif

  /* Register 'UART2_S2' initialization */
  #ifdef UART2_S2_VALUE
  UART2_S2 = UART2_S2_VALUE;
  #endif

  /* Clearing of the Rx flags */
  #ifdef UART2_CLEAR_RX_FLAGS
  (void)UART2_S1; /* Dummy read of the UART2_S1 register to clear flags */
  (void)UART2_D;  /* Dummy read of the UART2_D register to clear flags */
  #endif

  /* Register 'UART2_C3' initialization */
  #ifdef UART2_C3_VALUE
  UART2_C3 = UART2_C3_VALUE;
  #endif

  /* Register 'UART2_C2' initialization */
  #ifdef UART2_C2_VALUE_2
  UART2_C2 = UART2_C2_VALUE_2;
  #endif
}

#ifdef __cplusplus
}
#endif

/* END UART2_Init. */

/** ###################################################################
 **
 **     This file is a part of Processor Expert static initialization
 **     library for Freescale microcontrollers.
 **
 ** ################################################################### */
