/** ###################################################################
 **     Filename    : PORT_Init.c
 **     Processor   : MKE04Z128LD4
 **     Abstract    :
 **          This file implements the PORT module initialization
 **          according to the Peripheral Initialization settings, and
 **          defines interrupt service routines prototypes.
 **
 **     Contents    :
 **         Int   - void PORT_Init(void);
 **
 **     Copyright : 1997 - 2014 Freescale Semiconductor, Inc.
 **     All Rights Reserved.
 **
 **     Redistribution and use in source and binary forms, with or without modification,
 **     are permitted provided that the following conditions are met:
 **
 **     o Redistributions of source code must retain the above copyright notice, this list
 **       of conditions and the following disclaimer.
 **
 **     o Redistributions in binary form must reproduce the above copyright notice, this
 **       list of conditions and the following disclaimer in the documentation and/or
 **       other materials provided with the distribution.
 **
 **     o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 **       contributors may be used to endorse or promote products derived from this
 **       software without specific prior written permission.
 **
 **     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 **     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 **     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 **     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 **     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 **     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 **     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 **     ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 **     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 **     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 **     http: www.freescale.com
 **     mail: support@freescale.com
 ** ###################################################################*/

/*!
 * @file PORT_Init.c
 * @brief This file implements the PORT module initialization according to the
 *        Peripheral Initialization settings, and defines interrupt service
 *        routines prototypes.
 */


/* MODULE PORT_Init. */

#include "PORT_Init.h"
#include "MKE04Z1284.h"
#include "Init_Config.h"

#ifdef __cplusplus
extern "C" {
#endif

void PORT_Init(void) {

  /* Register 'PORT_IOFLT0' initialization */
  #if PORT_IOFLT0_MASK
    #if PORT_IOFLT0_MASK == 0xFFFFFFFF
  PORT_IOFLT0 = PORT_IOFLT0_VALUE;
    #elif PORT_IOFLT0_MASK == PORT_IOFLT0_VALUE
  PORT_IOFLT0 |= PORT_IOFLT0_VALUE;
    #elif PORT_IOFLT0_VALUE == 0
  PORT_IOFLT0 &= ~PORT_IOFLT0_MASK;
    #else
  PORT_IOFLT0 = (PORT_IOFLT0 & (~PORT_IOFLT0_MASK)) | PORT_IOFLT0_VALUE;
    #endif
  #elif defined(PORT_IOFLT0_VALUE)
  PORT_IOFLT0 = PORT_IOFLT0_VALUE;
  #endif

  /* Register 'PORT_IOFLT1' initialization */
  #if PORT_IOFLT1_MASK
    #if PORT_IOFLT1_MASK == 0xFFFFFFFF
  PORT_IOFLT1 = PORT_IOFLT1_VALUE;
    #elif PORT_IOFLT1_MASK == PORT_IOFLT1_VALUE
  PORT_IOFLT1 |= PORT_IOFLT1_VALUE;
    #elif PORT_IOFLT1_VALUE == 0
  PORT_IOFLT1 &= ~PORT_IOFLT1_MASK;
    #else
  PORT_IOFLT1 = (PORT_IOFLT1 & (~PORT_IOFLT1_MASK)) | PORT_IOFLT1_VALUE;
    #endif
  #elif defined(PORT_IOFLT1_VALUE)
  PORT_IOFLT1 = PORT_IOFLT1_VALUE;
  #endif

  /* Register 'PORT_PUE0' initialization */
  #if PORT_PUE0_MASK
    #if PORT_PUE0_MASK == 0xFFFFFFFF
  PORT_PUE0 = PORT_PUE0_VALUE;
    #elif PORT_PUE0_MASK == PORT_PUE0_VALUE
  PORT_PUE0 |= PORT_PUE0_VALUE;
    #elif PORT_PUE0_VALUE == 0
  PORT_PUE0 &= ~PORT_PUE0_MASK;
    #else
  PORT_PUE0 = (PORT_PUE0 & (~PORT_PUE0_MASK)) | PORT_PUE0_VALUE;
    #endif
  #elif defined(PORT_PUE0_VALUE)
  PORT_PUE0 = PORT_PUE0_VALUE;
  #endif

  /* Register 'PORT_PUE1' initialization */
  #if PORT_PUE1_MASK
    #if PORT_PUE1_MASK == 0xFFFFFFFF
  PORT_PUE1 = PORT_PUE1_VALUE;
    #elif PORT_PUE1_MASK == PORT_PUE1_VALUE
  PORT_PUE1 |= PORT_PUE1_VALUE;
    #elif PORT_PUE1_VALUE == 0
  PORT_PUE1 &= ~PORT_PUE1_MASK;
    #else
  PORT_PUE1 = (PORT_PUE1 & (~PORT_PUE1_MASK)) | PORT_PUE1_VALUE;
    #endif
  #elif defined(PORT_PUE1_VALUE)
  PORT_PUE1 = PORT_PUE1_VALUE;
  #endif

  /* Register 'PORT_PUE2' initialization */
  #if PORT_PUE2_MASK
    #if PORT_PUE2_MASK == 0xFFFFFFFF
  PORT_PUE2 = PORT_PUE2_VALUE;
    #elif PORT_PUE2_MASK == PORT_PUE2_VALUE
  PORT_PUE2 |= PORT_PUE2_VALUE;
    #elif PORT_PUE2_VALUE == 0
  PORT_PUE2 &= ~PORT_PUE2_MASK;
    #else
  PORT_PUE2 = (PORT_PUE2 & (~PORT_PUE2_MASK)) | PORT_PUE2_VALUE;
    #endif
  #elif defined(PORT_PUE2_VALUE)
  PORT_PUE2 = PORT_PUE2_VALUE;
  #endif

  /* Register 'PORT_HDRVE' initialization */
  #if PORT_HDRVE_MASK
    #if PORT_HDRVE_MASK == 0xFFFFFFFF
  PORT_HDRVE = PORT_HDRVE_VALUE;
    #elif PORT_HDRVE_MASK == PORT_HDRVE_VALUE
  PORT_HDRVE |= PORT_HDRVE_VALUE;
    #elif PORT_HDRVE_VALUE == 0
  PORT_HDRVE &= ~PORT_HDRVE_MASK;
    #else
  PORT_HDRVE = (PORT_HDRVE & (~PORT_HDRVE_MASK)) | PORT_HDRVE_VALUE;
    #endif
  #elif defined(PORT_HDRVE_VALUE)
  PORT_HDRVE = PORT_HDRVE_VALUE;
  #endif
}

#ifdef __cplusplus
}
#endif

/* END PORT_Init. */

/** ###################################################################
 **
 **     This file is a part of Processor Expert static initialization
 **     library for Freescale microcontrollers.
 **
 ** ################################################################### */
