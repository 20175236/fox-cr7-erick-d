/** ###################################################################
 **     Filename    : DAC6b1_Init.h
 **     Processor   : MKE04Z128LD4
 **     Abstract    :
 **          This file implements the DAC6b1 module initialization
 **          according to the Peripheral Initialization settings, and
 **          defines interrupt service routines prototypes.
 **
 **     Contents    :
 **         Int   - void DAC6b1_Init(void);
 **
 **     Copyright : 1997 - 2013 Freescale Semiconductor, Inc. All Rights Reserved.
 **     SOURCE DISTRIBUTION PERMISSIBLE as directed in End User License Agreement.
 **
 **     http      : www.freescale.com
 **     mail      : support@freescale.com
 ** ################################################################### */

/*!
 * @file DAC6b1_Init.h
 * @brief This file implements the DAC6b1 module initialization according to the
 *        Peripheral Initialization settings, and defines interrupt service
 *        routines prototypes.
 */


/* MODULE DAC6b1_Init. */


/* ----------------------------------------------------------------------------
   -- DAC6b1_Init module documentation
   ---------------------------------------------------------------------------- */

/*!
 * @addtogroup DAC6b1_Init_module DAC6b1_Init module documentation
 * @{
 */

#ifdef __cplusplus
extern "C" {
#endif

extern void DAC6b1_Init(void);

#ifdef __cplusplus
}
#endif

/*!
 * @}
 */ /* end of group DAC6b1_Init_module */


/* END DAC6b1_Init. */

/** ###################################################################
 **
 **     This file is a part of Processor Expert static initialization
 **     library for Freescale microcontrollers.
 **
 ** ################################################################### */
