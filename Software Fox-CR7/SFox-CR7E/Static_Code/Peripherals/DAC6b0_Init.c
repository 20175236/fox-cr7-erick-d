/** ###################################################################
 **     Filename    : DAC6b0_Init.c
 **     Processor   : MKE04Z128LD4
 **     Abstract    :
 **          This file implements the DAC6b0 module initialization
 **          according to the Peripheral Initialization settings, and
 **          defines interrupt service routines prototypes.
 **
 **     Contents    :
 **         Int   - void DAC6b0_Init(void);
 **
 **     Copyright : 1997 - 2013 Freescale Semiconductor, Inc. All Rights Reserved.
 **     SOURCE DISTRIBUTION PERMISSIBLE as directed in End User License Agreement.
 **
 **     http      : www.freescale.com
 **     mail      : support@freescale.com
 ** ################################################################### */

/*!
 * @file DAC6b0_Init.c
 * @brief This file implements the DAC6b0 module initialization according to the
 *        Peripheral Initialization settings, and defines interrupt service
 *        routines prototypes.
 */


/* MODULE DAC6b0_Init. */

#include "DAC6b0_Init.h"
#include "MKE04Z1284.h"
#include "Init_Config.h"

#ifdef __cplusplus
extern "C" {
#endif

void DAC6b0_Init(void) {

  /* Register 'ACMP0_C1' initialization */
  #ifdef ACMP0_C1_VALUE
  ACMP0_C1 = ACMP0_C1_VALUE;
  #endif
}

#ifdef __cplusplus
}
#endif

/* END DAC6b0_Init. */

/** ###################################################################
 **
 **     This file is a part of Processor Expert static initialization
 **     library for Freescale microcontrollers.
 **
 ** ################################################################### */
