/** ###################################################################
 **     Filename    : FTM0_Init.c
 **     Processor   : MKE04Z128LD4
 **     Abstract    :
 **          This file implements the FTM0 module initialization
 **          according to the Peripheral Initialization settings, and
 **          defines interrupt service routines prototypes.
 **
 **     Contents    :
 **         Int   - void FTM0_Init(void);
 **
 **     Copyright : 1997 - 2014 Freescale Semiconductor, Inc.
 **     All Rights Reserved.
 **
 **     Redistribution and use in source and binary forms, with or without modification,
 **     are permitted provided that the following conditions are met:
 **
 **     o Redistributions of source code must retain the above copyright notice, this list
 **       of conditions and the following disclaimer.
 **
 **     o Redistributions in binary form must reproduce the above copyright notice, this
 **       list of conditions and the following disclaimer in the documentation and/or
 **       other materials provided with the distribution.
 **
 **     o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 **       contributors may be used to endorse or promote products derived from this
 **       software without specific prior written permission.
 **
 **     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 **     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 **     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 **     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 **     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 **     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 **     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 **     ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 **     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 **     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 **     http: www.freescale.com
 **     mail: support@freescale.com
 ** ###################################################################*/

/*!
 * @file FTM0_Init.c
 * @brief This file implements the FTM0 module initialization according to the
 *        Peripheral Initialization settings, and defines interrupt service
 *        routines prototypes.
 */


/* MODULE FTM0_Init. */

#include "FTM0_Init.h"
#include "MKE04Z1284.h"
#include "Init_Config.h"

#ifdef __cplusplus
extern "C" {
#endif

void FTM0_Init(void) {
  (void)FTM0_SC;  /* Dummy read of the Control Status register to clear the interrupt flag */
  FTM0_SC = 0;  /* Stop the counter */
  #ifdef FTM0_C0SC
  (void)FTM0_C0SC; /* Dummy read of the Channel Status and Control 0 register to clear the interrupt flag */
  #endif
  #ifdef FTM0_C1SC
  (void)FTM0_C1SC; /* Dummy read of the Channel Status and Control 1 register to clear the interrupt flag */
  #endif
  #ifdef FTM0_C2SC
  (void)FTM0_C2SC; /* Dummy read of the Channel Status and Control 2 register to clear the interrupt flag */
  #endif
  #ifdef FTM0_C3SC
  (void)FTM0_C3SC; /* Dummy read of the Channel Status and Control 3 register to clear the interrupt flag */
  #endif
  #ifdef FTM0_C4SC
  (void)FTM0_C4SC; /* Dummy read of the Channel Status and Control 4 register to clear the interrupt flag */
  #endif
  #ifdef FTM0_C5SC
  (void)FTM0_C5SC; /* Dummy read of the Channel Status and Control 5 register to clear the interrupt flag */
  #endif
  #ifdef FTM0_C6SC
  (void)FTM0_C6SC; /* Dummy read of the Channel Status and Control 6 register to clear the interrupt flag */
  #endif
  #ifdef FTM0_C7SC
  (void)FTM0_C7SC; /* Dummy read of the Channel Status and Control 7 register to clear the interrupt flag */
  #endif

  FTM0_C0SC = 0;

  FTM0_C1SC = 0;

  /* Register 'FTM0_SC' initialization */
  #if FTM0_SC_MASK_2
    #if FTM0_SC_MASK_2 == 0xFFFFFFFF
  FTM0_SC = FTM0_SC_VALUE_2;
    #elif FTM0_SC_MASK_2 == FTM0_SC_VALUE_2
  FTM0_SC |= FTM0_SC_VALUE_2;
    #elif FTM0_SC_VALUE_2 == 0
  FTM0_SC &= ~FTM0_SC_MASK_2;
    #else
  FTM0_SC = (FTM0_SC & (~FTM0_SC_MASK_2)) | FTM0_SC_VALUE_2;
    #endif
  #elif defined(FTM0_SC_VALUE_2)
  FTM0_SC = FTM0_SC_VALUE_2;
  #endif

  /* Register 'FTM0_C0SC' initialization */
  #if FTM0_C0SC_MASK
    #if FTM0_C0SC_MASK == 0xFFFFFFFF
  FTM0_C0SC = FTM0_C0SC_VALUE;
    #elif FTM0_C0SC_MASK == FTM0_C0SC_VALUE
  FTM0_C0SC |= FTM0_C0SC_VALUE;
    #elif FTM0_C0SC_VALUE == 0
  FTM0_C0SC &= ~FTM0_C0SC_MASK;
    #else
  FTM0_C0SC = (FTM0_C0SC & (~FTM0_C0SC_MASK)) | FTM0_C0SC_VALUE;
    #endif
  #elif defined(FTM0_C0SC_VALUE)
  FTM0_C0SC = FTM0_C0SC_VALUE;
  #endif

  /* Register 'FTM0_C1SC' initialization */
  #if FTM0_C1SC_MASK
    #if FTM0_C1SC_MASK == 0xFFFFFFFF
  FTM0_C1SC = FTM0_C1SC_VALUE;
    #elif FTM0_C1SC_MASK == FTM0_C1SC_VALUE
  FTM0_C1SC |= FTM0_C1SC_VALUE;
    #elif FTM0_C1SC_VALUE == 0
  FTM0_C1SC &= ~FTM0_C1SC_MASK;
    #else
  FTM0_C1SC = (FTM0_C1SC & (~FTM0_C1SC_MASK)) | FTM0_C1SC_VALUE;
    #endif
  #elif defined(FTM0_C1SC_VALUE)
  FTM0_C1SC = FTM0_C1SC_VALUE;
  #endif

  /* Register 'FTM0_C0V' initialization */
  #if FTM0_C0V_MASK
    #if FTM0_C0V_MASK == 0xFFFFFFFF
  FTM0_C0V = FTM0_C0V_VALUE;
    #elif FTM0_C0V_MASK == FTM0_C0V_VALUE
  FTM0_C0V |= FTM0_C0V_VALUE;
    #elif FTM0_C0V_VALUE == 0
  FTM0_C0V &= ~FTM0_C0V_MASK;
    #else
  FTM0_C0V = (FTM0_C0V & (~FTM0_C0V_MASK)) | FTM0_C0V_VALUE;
    #endif
  #elif defined(FTM0_C0V_VALUE)
  FTM0_C0V = FTM0_C0V_VALUE;
  #endif

  /* Register 'FTM0_C1V' initialization */
  #if FTM0_C1V_MASK
    #if FTM0_C1V_MASK == 0xFFFFFFFF
  FTM0_C1V = FTM0_C1V_VALUE;
    #elif FTM0_C1V_MASK == FTM0_C1V_VALUE
  FTM0_C1V |= FTM0_C1V_VALUE;
    #elif FTM0_C1V_VALUE == 0
  FTM0_C1V &= ~FTM0_C1V_MASK;
    #else
  FTM0_C1V = (FTM0_C1V & (~FTM0_C1V_MASK)) | FTM0_C1V_VALUE;
    #endif
  #elif defined(FTM0_C1V_VALUE)
  FTM0_C1V = FTM0_C1V_VALUE;
  #endif

  /* Register 'FTM0_EXTTRIG' initialization */
  #if FTM0_EXTTRIG_MASK
    #if FTM0_EXTTRIG_MASK == 0xFFFFFFFF
  FTM0_EXTTRIG = FTM0_EXTTRIG_VALUE;
    #elif FTM0_EXTTRIG_MASK == FTM0_EXTTRIG_VALUE
  FTM0_EXTTRIG |= FTM0_EXTTRIG_VALUE;
    #elif FTM0_EXTTRIG_VALUE == 0
  FTM0_EXTTRIG &= ~FTM0_EXTTRIG_MASK;
    #else
  FTM0_EXTTRIG = (FTM0_EXTTRIG & (~FTM0_EXTTRIG_MASK)) | FTM0_EXTTRIG_VALUE;
    #endif
  #elif defined(FTM0_EXTTRIG_VALUE)
  FTM0_EXTTRIG = FTM0_EXTTRIG_VALUE;
  #endif

  /* Register 'FTM0_MOD' initialization */
  #if FTM0_MOD_MASK
    #if FTM0_MOD_MASK == 0xFFFFFFFF
  FTM0_MOD = FTM0_MOD_VALUE;
    #elif FTM0_MOD_MASK == FTM0_MOD_VALUE
  FTM0_MOD |= FTM0_MOD_VALUE;
    #elif FTM0_MOD_VALUE == 0
  FTM0_MOD &= ~FTM0_MOD_MASK;
    #else
  FTM0_MOD = (FTM0_MOD & (~FTM0_MOD_MASK)) | FTM0_MOD_VALUE;
    #endif
  #elif defined(FTM0_MOD_VALUE)
  FTM0_MOD = FTM0_MOD_VALUE;
  #endif

  FTM0_CNT &= ~0xFFFF;

  /* Register 'FTM0_SC' initialization */
  #if FTM0_SC_MASK_3
    #if FTM0_SC_MASK_3 == 0xFFFFFFFF
  FTM0_SC = FTM0_SC_VALUE_3;
    #elif FTM0_SC_MASK_3 == FTM0_SC_VALUE_3
  FTM0_SC |= FTM0_SC_VALUE_3;
    #elif FTM0_SC_VALUE_3 == 0
  FTM0_SC &= ~FTM0_SC_MASK_3;
    #else
  FTM0_SC = (FTM0_SC & (~FTM0_SC_MASK_3)) | FTM0_SC_VALUE_3;
    #endif
  #elif defined(FTM0_SC_VALUE_3)
  FTM0_SC = FTM0_SC_VALUE_3;
  #endif
}

#ifdef __cplusplus
}
#endif

/* END FTM0_Init. */

/** ###################################################################
 **
 **     This file is a part of Processor Expert static initialization
 **     library for Freescale microcontrollers.
 **
 ** ################################################################### */
