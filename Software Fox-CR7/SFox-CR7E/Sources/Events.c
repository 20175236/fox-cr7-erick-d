/* ###################################################################
**     Filename    : Events.c
**     Project     : SFox-CR7E
**     Processor   : MKE04Z128VLD4
**     Component   : Events
**     Version     : Driver 01.00
**     Compiler    : Keil ARM C/C++ Compiler
**     Date/Time   : 2021-05-01, 12:02, # CodeGen: 0
**     Abstract    :
**         This is user's event module.
**         Put your event handler code here.
**     Settings    :
**     Contents    :
**         Cpu_OnNMI - void Cpu_OnNMI(void);
**
** ###################################################################*/
/*!
** @file Events.c
** @version 01.00
** @brief
**         This is user's event module.
**         Put your event handler code here.
*/         
/*!
**  @addtogroup Events_module Events module documentation
**  @{
*/         
/* MODULE Events */

#include "Cpu.h"
#include "Events.h"
#include "Init_Config.h"
#include "PDD_Includes.h"

#ifdef __cplusplus
extern "C" {
#endif 


/* User includes (#include below this line is not maintained by Processor Expert) */
extern volatile struct _Sys_IO{
	  unsigned int IN_S_DOOR:1;
	  unsigned int IN_S_PRESEN:1;
	  unsigned int IN_S_TARG:1;
	  unsigned int IN_S_WIN:1;
	  unsigned int ACT_LIMPIEZA:1;
	}Sys_IO;
/*
** ===================================================================
**     Event       :  Cpu_OnNMI (module Events)
**
**     Component   :  Cpu [MKE04Z128LK4]
*/
/*!
**     @brief
**         This event is called when the Non maskable interrupt had
**         occurred. This event is automatically enabled when the [NMI
**         interrupt] property is set to 'Enabled'.
*/
/* ===================================================================*/
void Cpu_OnNMI(void)
{
  /* Write your code here ... */
}

/*
** ===================================================================
**     Event       :  T1hz_OnInterrupt (module Events)
**
**     Component   :  T1hz [TimerInt]
**     Description :
**         When a timer interrupt occurs this event is called (only
**         when the component is enabled - <Enable> and the events are
**         enabled - <EnableEvent>). This event is enabled only if a
**         <interrupt service/event> is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
void T1hz_OnInterrupt(void)
{
  /* Write your code here ... */
	LED_NegVal();
	if(!S_Door_GetVal())
	{
		Sys_IO.IN_S_DOOR = TRUE;
		CT1++;
	}
	else
	{
		Sys_IO.IN_S_DOOR = FALSE;
		CT1=0;
	}
	if(S_Precen_GetVal())
	{
		Sys_IO.IN_S_PRESEN = TRUE;
	}
	else
	{
		Sys_IO.IN_S_PRESEN = FALSE;
	}
	if(S_Targ_GetVal())
	{
		Sys_IO.IN_S_TARG = TRUE;
	}
	else
	{
		Sys_IO.IN_S_TARG = FALSE;
	}
	if(S_Win_GetVal())
	{
		Sys_IO.IN_S_WIN = TRUE;
	}
	else
	{
		Sys_IO.IN_S_WIN = FALSE;
	}
	if((Sys_IO.IN_S_PRESEN == TRUE) && (Sys_IO.IN_S_TARG == FALSE))
	{
		Sys_IO.ACT_LIMPIEZA = TRUE;
	}
	else
	{
		Sys_IO.ACT_LIMPIEZA = FALSE;
	}
}

/*
** ===================================================================
**     Event       :  Cpu_OnHardFault (module Events)
**
**     Component   :  Cpu [MKE04Z128LD4]
*/
/*!
**     @brief
**         This event is called when the Hard Fault exception had
**         occurred. This event is automatically enabled when the [Hard
**         Fault] property is set to 'Enabled'.
*/
/* ===================================================================*/
void Cpu_OnHardFault(void)
{
  /* Write your code here ... */
}

/*
** ===================================================================
**     Event       :  T_PID_OnInterrupt (module Events)
**
**     Component   :  T_PID [TimerInt]
**     Description :
**         When a timer interrupt occurs this event is called (only
**         when the component is enabled - <Enable> and the events are
**         enabled - <EnableEvent>). This event is enabled only if a
**         <interrupt service/event> is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
void T_PID_OnInterrupt(void)
{
  /* Write your code here ... */
	PID1_Control();
}

/* END Events */

#ifdef __cplusplus
}  /* extern "C" */
#endif 

/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.4 [05.09]
**     for the Freescale Kinetis series of microcontrollers.
**
** ###################################################################
*/
