/* ###################################################################
**     Filename    : main.c
**     Project     : SFox-CR7E
**     Processor   : MKE04Z128VLD4
**     Version     : Driver 01.01
**     Compiler    : Keil ARM C/C++ Compiler
**     Date/Time   : 2021-05-01, 12:02, # CodeGen: 0
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.01
** @brief
**         Main module.
**         This module contains user's application code.
*/         
/*!
**  @addtogroup main_module main module documentation
**  @{
*/         
/* MODULE main */


/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "Events.h"
#include "S_Door.h"
#include "BitIoLdd1.h"
#include "S_Win.h"
#include "BitIoLdd2.h"
#include "S_Precen.h"
#include "BitIoLdd3.h"
#include "S_Targ.h"
#include "BitIoLdd4.h"
#include "T1hz.h"
#include "TimerIntLdd1.h"
#include "TU1.h"
#include "T_PID.h"
#include "TimerIntLdd2.h"
#include "TU2.h"
#include "PID1.h"
#include "MCUC1.h"
#include "FAN1.h"
#include "BitIoLdd5.h"
#include "FAN2.h"
#include "BitIoLdd6.h"
#include "FAN3.h"
#include "BitIoLdd7.h"
#include "OUT1.h"
#include "BitIoLdd8.h"
#include "OUT2.h"
#include "BitIoLdd9.h"
#include "LED.h"
#include "BitIoLdd10.h"
#include "AS1.h"
#include "ASerialLdd1.h"
#include "DIP1.h"
#include "BitIoLdd11.h"
#include "DIP2.h"
#include "BitIoLdd12.h"
#include "DIP3.h"
#include "BitIoLdd13.h"
#include "DIP4.h"
#include "BitIoLdd14.h"
#include "DIP5.h"
#include "BitIoLdd15.h"
#include "DIP6.h"
#include "BitIoLdd16.h"
#include "DIP7.h"
#include "BitIoLdd17.h"
#include "DIP8.h"
#include "BitIoLdd18.h"
#include "DIP9.h"
#include "BitIoLdd19.h"
#include "DIP10.h"
#include "BitIoLdd20.h"
#include "DIP11.h"
#include "BitIoLdd21.h"
#include "DIP12.h"
#include "BitIoLdd22.h"
/* Including shared modules, which are used for whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "PDD_Includes.h"
#include "Init_Config.h"

/* User includes (#include below this line is not maintained by Processor Expert) */
#define E_Verificacion 	0
#define E_ECO       	1
#define E_Ocupada		2
#define E_Desocupada	3
#define E_ERROR     	4
#define E_Limpieza   	5

unsigned int Func_E_Verificacion(void);
unsigned int Func_E_ECO(void);
unsigned int Func_E_Ocupada(void);
unsigned int Func_E_Desocupada(void);
unsigned int Func_E_ERROR(void);
unsigned int Func_E_Limpieza(void);
unsigned int CT1;

volatile struct _Sys_IO{
  unsigned int IN_S_DOOR:1;
  unsigned int IN_S_PRESEN:1;
  unsigned int IN_S_TARG:1;
  unsigned int IN_S_WIN:1;
  unsigned int ACT_LIMPIEZA:1;
}Sys_IO;

struct _EST_SIS{
  unsigned int EST_ACTUAL;
  unsigned int EST_NEXT;
  unsigned int EST_BACK;
}EST_SIS;


/*lint -save  -e970 Disable MISRA rule (6.3) checking. */
int main(void)
/*lint -restore Enable MISRA rule (6.3) checking. */
{
  /* Write your local variable definition here */

  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  PE_low_level_init();
  /*** End of Processor Expert internal initialization.                    ***/

  /* Write your code here */
  /* For example: for(;;) { } */
  EST_SIS.EST_NEXT = E_Verificacion;

  for(;;){
	  if(EST_SIS.EST_NEXT == E_Verificacion)
	  		{
	  		  EST_SIS.EST_NEXT = Func_E_Verificacion();
	  		}
	  if(EST_SIS.EST_NEXT == E_ECO)
		{
		  EST_SIS.EST_NEXT = Func_E_ECO();
		}
	  if(EST_SIS.EST_NEXT == E_Ocupada)
		{
		  EST_SIS.EST_NEXT = Func_E_Ocupada();
		}
	  if(EST_SIS.EST_NEXT == E_Desocupada)
		{
		  EST_SIS.EST_NEXT = Func_E_Desocupada();
		}
	  if(EST_SIS.EST_NEXT == E_ERROR)
		{
		  EST_SIS.EST_NEXT = Func_E_ERROR();
		}
	  if(EST_SIS.EST_NEXT == E_Limpieza)
		{
		  EST_SIS.EST_NEXT = Func_E_Limpieza();
    }

  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;){}
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.4 [05.09]
**     for the Freescale Kinetis series of microcontrollers.
**
** ###################################################################
*/

unsigned int Func_E_Verificacion(void)
{
	// todo que hacemos con las seriales y la pantalla

	//desactivar todos los relay
	FAN1_ClrVal();
	FAN2_ClrVal();
	FAN3_ClrVal();
	OUT1_ClrVal();
	OUT2_ClrVal();
	//azerar todos los valores
	Sys_IO.ACT_LIMPIEZA =	FALSE;
	Sys_IO.IN_S_DOOR 	= 	FALSE;
	Sys_IO.IN_S_PRESEN 	= 	FALSE;
	Sys_IO.IN_S_TARG 	= 	FALSE;
	Sys_IO.IN_S_WIN 	= 	FALSE;
	// todo aqui vamos a verifericar los leds

	//Aca detectarems si hay alguien en la habitacion
	if(Sys_IO.IN_S_PRESEN == TRUE)
	{
		return E_Ocupada;
	}

	if(Sys_IO.IN_S_TARG == FALSE)
	{
		return E_Desocupada;
	}

}
unsigned int Func_E_ECO(void)
{
	for(;;)
	{
		if((Sys_IO.IN_S_PRESEN == FALSE)&&(Sys_IO.IN_S_TARG == FALSE)&&(Sys_IO.ACT_LIMPIEZA == FALSE))
		{
			return E_Desocupada;
		}
		if((Sys_IO.IN_S_WIN == FALSE)&&(Sys_IO.IN_S_PRESEN == TRUE)&&(Sys_IO.IN_S_TARG == TRUE))
		{
			return E_Ocupada;
		}
	}
}
unsigned int Func_E_Ocupada(void)
{
	for(;;)
	{
		if((Sys_IO.IN_S_WIN==TRUE)||(Sys_IO.ACT_LIMPIEZA==TRUE)||(TIEMPO PUESTA ABIERTA)){
			return E_ECO;
		}
		if((Sys_IO.IN_S_PRESEN == FALSE)&&(Sys_IO.IN_S_TARG == FALSE))
		{
			return E_Desocupada;
		}
	}
}
unsigned int Func_E_Desocupada(void){
	for(;;)
		{
			if(Sys_IO.IN_S_TARG == TRUE)
			{
				return E_Ocupada;
			}
			if(Sys_IO.ACT_LIMPIEZA == TRUE)
			{
				return E_Limpieza;
			}
		}
}
unsigned int Func_E_ERROR(void)
{
	for(;;)
	{

	}
}
unsigned int Func_E_Limpieza(void)
{
	if(Sys_IO.IN_S_TARG == FALSE || TIEMPO AGOTADO)
	{
		return E_Desocupada;
	}
}

