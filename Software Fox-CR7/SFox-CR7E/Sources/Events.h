/* ###################################################################
**     Filename    : Events.h
**     Project     : SFox-CR7E
**     Processor   : MKE04Z128VLD4
**     Component   : Events
**     Version     : Driver 01.00
**     Compiler    : Keil ARM C/C++ Compiler
**     Date/Time   : 2021-05-01, 12:02, # CodeGen: 0
**     Abstract    :
**         This is user's event module.
**         Put your event handler code here.
**     Settings    :
**     Contents    :
**         Cpu_OnNMI - void Cpu_OnNMI(void);
**
**Copyright : 1997 - 2014 Freescale Semiconductor, Inc. 
**All Rights Reserved.
**
**Redistribution and use in source and binary forms, with or without modification,
**are permitted provided that the following conditions are met:
**
**o Redistributions of source code must retain the above copyright notice, this list
**  of conditions and the following disclaimer.
**
**o Redistributions in binary form must reproduce the above copyright notice, this
**  list of conditions and the following disclaimer in the documentation and/or
**  other materials provided with the distribution.
**
**o Neither the name of Freescale Semiconductor, Inc. nor the names of its
**  contributors may be used to endorse or promote products derived from this
**  software without specific prior written permission.
**
**THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
**ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
**WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
**DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
**ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
**(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
**ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
**(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
**SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
**http: www.freescale.com
**mail: support@freescale.com
** ###################################################################*/
/*!
** @file Events.h
** @version 01.00
** @brief
**         This is user's event module.
**         Put your event handler code here.
*/         
/*!
**  @addtogroup Events_module Events module documentation
**  @{
*/         

#ifndef __Events_H
#define __Events_H
/* MODULE Events */

#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "S_Door.h"
#include "BitIoLdd1.h"
#include "S_Win.h"
#include "BitIoLdd2.h"
#include "S_Precen.h"
#include "BitIoLdd3.h"
#include "S_Targ.h"
#include "BitIoLdd4.h"
#include "T1hz.h"
#include "TimerIntLdd1.h"
#include "TU1.h"
#include "T_PID.h"
#include "TimerIntLdd2.h"
#include "TU2.h"
#include "PID1.h"
#include "MCUC1.h"
#include "FAN1.h"
#include "BitIoLdd5.h"
#include "FAN2.h"
#include "BitIoLdd6.h"
#include "FAN3.h"
#include "BitIoLdd7.h"
#include "OUT1.h"
#include "BitIoLdd8.h"
#include "OUT2.h"
#include "BitIoLdd9.h"
#include "LED.h"
#include "BitIoLdd10.h"
#include "AS1.h"
#include "ASerialLdd1.h"
#include "DIP1.h"
#include "BitIoLdd11.h"
#include "DIP2.h"
#include "BitIoLdd12.h"
#include "DIP3.h"
#include "BitIoLdd13.h"
#include "DIP4.h"
#include "BitIoLdd14.h"
#include "DIP5.h"
#include "BitIoLdd15.h"
#include "DIP6.h"
#include "BitIoLdd16.h"
#include "DIP7.h"
#include "BitIoLdd17.h"
#include "DIP8.h"
#include "BitIoLdd18.h"
#include "DIP9.h"
#include "BitIoLdd19.h"
#include "DIP10.h"
#include "BitIoLdd20.h"
#include "DIP11.h"
#include "BitIoLdd21.h"
#include "DIP12.h"
#include "BitIoLdd22.h"

#ifdef __cplusplus
extern "C" {
#endif 

/*
** ===================================================================
**     Event       :  Cpu_OnNMI (module Events)
**
**     Component   :  Cpu [MKE04Z128LK4]
*/
/*!
**     @brief
**         This event is called when the Non maskable interrupt had
**         occurred. This event is automatically enabled when the [NMI
**         interrupt] property is set to 'Enabled'.
*/
/* ===================================================================*/
void Cpu_OnNMI(void);


/*
** ===================================================================
**     Event       :  T1hz_OnInterrupt (module Events)
**
**     Component   :  T1hz [TimerInt]
**     Description :
**         When a timer interrupt occurs this event is called (only
**         when the component is enabled - <Enable> and the events are
**         enabled - <EnableEvent>). This event is enabled only if a
**         <interrupt service/event> is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
void T1hz_OnInterrupt(void);

/*
** ===================================================================
**     Event       :  Cpu_OnHardFault (module Events)
**
**     Component   :  Cpu [MKE04Z128LD4]
*/
/*!
**     @brief
**         This event is called when the Hard Fault exception had
**         occurred. This event is automatically enabled when the [Hard
**         Fault] property is set to 'Enabled'.
*/
/* ===================================================================*/
void Cpu_OnHardFault(void);

/*
** ===================================================================
**     Event       :  T_PID_OnInterrupt (module Events)
**
**     Component   :  T_PID [TimerInt]
**     Description :
**         When a timer interrupt occurs this event is called (only
**         when the component is enabled - <Enable> and the events are
**         enabled - <EnableEvent>). This event is enabled only if a
**         <interrupt service/event> is enabled.
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
void T_PID_OnInterrupt(void);

/* END Events */

#ifdef __cplusplus
}  /* extern "C" */
#endif 

#endif 
/* ifndef __Events_H*/
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.4 [05.09]
**     for the Freescale Kinetis series of microcontrollers.
**
** ###################################################################
*/
